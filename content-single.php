<?php
/**
 * @package aserica
 */
?>

<article id="post-<?php the_ID(); ?>"  class="panel panel-default">
	<header class="panel-heading">
		<h1 class="panel-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="panel-body">
		<?php the_content(); ?>
		<?
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'aserica' ),
			'after'  => '</div>',
			) );

			?>
		</div><!-- .entry-content -->
		<div class="panel-footer">
			<footer class="small">
				
					<?php aserica_posted_on(); ?>
				
						<?php
						/* translators: used between list items, there is a space after the comma */
						$category_list = get_the_category_list( __( ', ', 'aserica' ) );

						/* translators: used between list items, there is a space after the comma */
						$tag_list = get_the_tag_list( '', __( ', ', 'aserica' ) );

						if ( ! aserica_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
							if ( '' != $tag_list ) {
								$meta_text = __( '<i class="fa fa-tags"></i> %2$s. ', 'aserica' );
							} else {
								$meta_text = __( 'Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'aserica' );
							}

						} else {
				// But this blog has loads of categories so we should probably display them here
							if ( '' != $tag_list ) {
								$meta_text = __( '<i class="fa fa-sitemap"></i> %1$s  <i class="fa fa-tags"></i> %2$s.', 'aserica' );
							} else {
								$meta_text = __( 'This entry was posted in %1$s. ', 'aserica' );
							}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
				);
				?>

				<?php edit_post_link( __( 'Edit <i class="fa fa-pencil-square-o"></i>', 'aserica' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
		</div>			
		</article><!-- #post-## -->