<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package aserica
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php if(of_get_option('favicon')) { ?>
	<link rel="shortcut icon" type="image/png" href="<?php echo of_get_option('favicon'); ?>">
	<?php } else{?>
	<link rel="shortcut icon" type="image/png" href="http://favicon-generator.org/favicons/2013-12-27/d85379e06f1b62b99d05fa32ec82c6d2.ico">
	<?php }?>
	<!-- Bootstrap-->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<!-- Font Awesome-->
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<!--Web Fonts-->
	<link href='http://fonts.googleapis.com/css?family=Alegreya' rel='stylesheet' type='text/css'>
	<!-- Custom-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/kokarat.css" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<div class="header">
		<div class="row">
			<div class="container">

				<div class="text-center">
					<?php if(of_get_option('header_image_logo')) { ?>
					<img class="i" src="<?php echo of_get_option('header_image_logo'); ?>" alt="">
					<?php } else{?>
					<img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-black.png" alt="">
					<?php }?>
				</div>

				<menu class="text-center">
					<li><a href="<?php echo get_site_url(); ?>/page-home">HOME</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-aserican">ASERICAN</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-fashion">FASHION</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-news">NEWS</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-exclusive">EXCLUSIVE</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-introducing">INTRODUCING</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-days-places">DAYS AND PLACES</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-more">MORE</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-about-us">ABOUT US</a></li>
				</menu>
				<div class="space5"></div>
				<div class="black-line"></div>
				<div class="line-normal-blod"></div>
				

				<div class="row">
					<div class="col-md-3">	
						<span class="subscribe-header"><a href="<?php echo get_site_url(); ?>/page-more">SUBSCRIBE TO ASERICA</a></span>
					</div>
					<div class="col-md-3"></div>
					<div class="col-md-4">
						<div class="header-social">
							<span class="pull-right"><a href="<?php echo of_get_option('facebook'); ?>" target="_blank">FACEBOOK </a>/ <a href="<?php echo of_get_option('twitter'); ?>" target="_blank">TWITTER </a>/ <a href="<?php echo of_get_option('instagram'); ?>" target="_blank">INSTAGRAM </a> / <a href="mailto:info@aserica.com" target="_blank">CONTACT </a></span>

						</div>
					</div>
					<div class="col-md-2 header-social">
						<form role="form" method="get" class="pull-right" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="text" class="pull-right" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'aserica' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">	
						</form>
					</div>
				</div>



				<div class="space10"></div>
				<div class="top-featuring">
					<?php echo of_get_option('header_text')?>
				</div>	
			</div>
		</div>
	</div>	
	<div class="main container">
