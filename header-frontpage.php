<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package aserica
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php if(of_get_option('favicon')) { ?>
	<link rel="shortcut icon" type="image/png" href="<?php echo of_get_option('favicon'); ?>">
	<?php } else{?>
	<link rel="shortcut icon" type="image/png" href="http://favicon-generator.org/favicons/2013-12-27/d85379e06f1b62b99d05fa32ec82c6d2.ico">
	<?php }?>
	<!-- Bootstrap-->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<!-- Font Awesome-->
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<!--Web Fonts-->
	<link href='http://fonts.googleapis.com/css?family=Alegreya' rel='stylesheet' type='text/css'>
	<!-- Custom-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/kokarat.css" />
	<?php wp_head(); ?>
</head>

<body class="front-body">

	<div class="navbar transparent  navbar-fixed-top">
		<nav class="navbar-inner">
			<div class="container text-center"><img src="https://farm8.staticflickr.com/7565/16316276435_a323e27ee9_o.png" alt=""></div>
		</div>
	</div>