<?php
/**
 * The Template for displaying all single posts.
 *
 * @package aserica
 */

get_header( 'frontpage' );?>

<?php
// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query
$args = array(
    'post_type'         => 'coverpage',
    'orderby'           => 'menu_order',
    'order'             => 'ASC',
    'posts_per_page'  => 4
    );
$the_query = new WP_Query( $args );

if($the_query->post_count>0){
    $i = 0;
    while ( $the_query->have_posts() ) : $the_query->the_post();
    $image_type = array("tech", "nature", "arch", "people");
    ?>
    <?php 
     $cover_image = get_post_meta( $post->ID, 'aserica_cover_image', true);
    if($cover_image != '') :
        ?>

    <?php if($i == 0){?>

    <a href="#" id="spnbottom"><img class="img-responsive" src="<?=$cover_image?>" alt=""></a>

    <?php }else{?>

    <a href="<?php echo get_site_url(); ?>/page-home"><img class="img-responsive" src="<?=$cover_image?>" alt=""></a>

    <?php }?>

<?php else : ?> 
 <?php if($i == 0){?>

 <a  href="#" id="spnbottom"><img class="img-responsive" src="http://placeimg.com/2000/800/<?=$image_type[rand(0,3)]?>" alt=""></a>

 <?php }else{?>

  <a href="<?php echo get_site_url(); ?>/page-home"><img class="img-responsive" src="http://placeimg.com/2000/800/<?=$image_type[rand(0,3)]?>" alt=""></a>

<?php }?>
<?php endif ;?>     
<?php
$i++;
endwhile;     
        }// endif     
        /* 
         * Restore original Post Data
         */
        wp_reset_postdata();
        ?>

        <!-- /.custom_post_types -->
        <?php get_footer(); ?>