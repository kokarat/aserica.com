<?php
/**
 * Template Name: INTRODUCING
 *
 * @package aserica
 */
$paged = ( get_query_var( 'paged' ) ) ? get_query_var('paged') : 1;
get_header(); ?>

<div class="row"><p class="page-title text-center">INTRODUCING</p></div>

<!--Pages Titles-->
<div class="row">
	<div class="text-center">
		<p class="page-sub-titile">SELECTED WORKS</p>
		<div class="col-md-1"></div>
		<div class="col-md-10"><?php echo of_get_option('introducing')?></div>
	</div>
	<div class="col-md-1"></div>	
</div>

<div class="space20"></div>

<!--End Pages Titles-->


<!-- Custom_Post_Type -->
<?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

$args = array(
	'post_type'	    => 'introducing',
	'posts_per_page'  => 3,
	'orderby' => 'date',
	'order' => 'DESC',
	'paged'=>$paged
	);

$lr_check = 0;
$the_query = new WP_Query( $args );

if($the_query->post_count>0){
	
	$i	= 0;
	$col 	= 1;
	
	while ( $the_query->have_posts() ) : $the_query->the_post();
	?>


	
	<?php $lr_check++;

	if($i%$col===0){ echo ' <div class="">';}
	
	?>
	
	<?php 
	$lr = $lr_check%2;
	if($lr  == 1) {?>


	<div class="row">
		<div class="space20"></div>
		<div class="col-md-9"><?php  the_post_thumbnail('full-image', array('class' => 'img-responsive')); ?></div>
		<div class="col-md-3"><span class="big-bold"><?=get_post_meta( $post->ID, 'aserica_bold_firstname', true );?><br/> <?=get_post_meta( $post->ID, 'aserica_bold_lastname', true );?> </span> <br/><span class="content-front-page"> <?=iconv_substr(wpautop(get_post_meta( $post->ID, 'aserica_content_text', true )),0,950, "UTF-8")."...";?>
		</span>
		<br/>
		<br/>
		<p>
			<span class="big-bold-yellow"> <a href="<?=the_permalink();?>">SEE>> </a></span>
		</p>
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
	</div>
</div>
<div class="space10"></div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>

<?php }else{?>
<div class="row">
	<div class="space20"></div>
	<div class="col-md-3"><span class="big-bold"><?=get_post_meta( $post->ID, 'aserica_bold_firstname', true );?><br/> <?=get_post_meta( $post->ID, 'aserica_bold_lastname', true );?></span> <br/><span class="content-front-page"> <?=iconv_substr(wpautop(get_post_meta( $post->ID, 'aserica_content_text', true )),0,950, "UTF-8")."...";?>
	</span>
	<br/>
	<br/>
	<p>
		<span class="big-bold-yellow"> <a href="<?=the_permalink();?>">SEE>> </a></span>
	</p>
</div>
<div class="col-md-9"><?php  the_post_thumbnail('full-image', array('class' => 'img-responsive')); ?></div>
</div>

<div class="row">
	<div class="space20"></div>
	<div class="col-md-12">
		<div class="pull-left botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
	</div>
</div>
<div class="space10"></div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>
<?php }?>

<?php

if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
}

$i++;
$total_pages = $the_query->max_num_pages;
if ($total_pages != $paged) {
	$chang_page = $paged+1;
} else {
	$chang_page = $paged-1;
}
endwhile;
?>

<?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

<?php
}
	/* 
	 * Restore original Post Data
	 */
	wp_reset_postdata();
	?>

	<!-- /.custom_post_types -->
<div class="space20"></div>

	<!-- INTRODUCING Pagination -->
	<div class="row">
		<div class="col-md-3"><span class="big-bold"> <?=of_get_option('introducing_page_bold')?> </span> 
			<br/> 
			<span class="content-front-page"> 
				<?=of_get_option('introducing_page_col')?>
			</span>
			<br/>
			<br/>
			<p>
				<span class="big-bold-yellow"> <a href="<?php echo get_site_url(); ?>/page-introducing/page/<?=$chang_page?>">MORE>> <br/>INTRODUCING</a></span>
			</p>
		</div>


		<!-- Ramdom -->	
		<div class="col-md-9">

			<?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

			$args = array(
				'post_type' => 'introducing',
				'posts_per_page'  => 3,
				'orderby' => 'date',
				'order' => 'DESC',
				'paged'=>$paged+1
				);
			$the_query = new WP_Query( $args );

			if($the_query->post_count>0){

				$i	= 0;
				$col 	= 2;

				while ( $the_query->have_posts() ) : $the_query->the_post();
				if($i<2){
					if($i%$col===0){ echo ' <div class="row">';}

					?>

					<div class="col-md-<?php echo (12/$col); ?> <?php echo get_post_type(); ?>">

						<a href="<?=the_permalink();?>"><?php  the_post_thumbnail('big-image', array('class' => 'img-responsive')); ?></a>
						<div class="space5"></div>
						<span class="news">INTRODUCING</span><br/>
						<span class="page-the-title"><?php the_title();?> </span><br/>
						
						<p><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></p> 

					</div>

					<?php

					if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
				}

				$i++;
			}
			endwhile;
			?>

			<?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

			<?php
		}
	/* 
	 * Restore original Post Data
	 */
	wp_reset_postdata();
	?>



</div>
</div>

<div class="row">
	<div class="col-md-12">
		<span class="front-page-under">INTRODUCING</span>
		<br/>
		<?=of_get_option('introducing')?>
		<div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
	</div>

</div>
<div class="space10"></div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>

<!-- End INTRODUCING -->

<div class="space40"></div>




<?php get_footer(); ?>
