<?php
/* 
* @Author: Anat Kokarat
* @Date:   2014-06-27 16:02:44
* @Last Modified by:   Anat Kokarat
* @Last Modified time: 2014-07-13 21:54:46
*/
?>

<?php
/**
 * Template Name: About US
 *
 * @package aserica
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	
	<div class="space20"></div>
	<!--Pages Titles-->
	<div class="row">
		<div class="text-center">
			<p class="page-title text-center">ABOUT US</p>
			<p class="page-sub-titile"><?php the_title();?></p>
			<div class="col-md-1"></div>
			<div class="col-md-10"><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></div>
		</div>
		<div class="col-md-1"></div>	
	</div>

	<div class="space20"></div>

	<!--End Pages Titles-->


<div class="col-md-12">
	<div class="space20"></div>
	
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<span class="fashion-under-slide-title"><strong> <?=get_post_meta( $post->ID, 'aserica_photography_author', true );?></strong></span>
			<p><?=the_content();?></p>
			</div>
			<div class="col-md-1"></div>
		</div>

	</div>


<div class="space20"></div>
<div class="row">
	<div class="col-md-12"><p class="pull-right"><span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  <span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></p></div> 
</div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>

<?php endwhile; // end of the loop. ?>

<div class="space40"></div>
<?php get_footer(); ?>