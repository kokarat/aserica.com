<?php
/**
 * Template Name: FASHION
 *
 * @package aserica
 */
$paged = ( get_query_var( 'paged' ) ) ? get_query_var('paged') : 1;
get_header(); ?>


<!-- Custom_Post_Type -->
<div class="row"><p class="page-title text-center">FASHION</p></div>
<?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

$args = array(
	'post_type'	    => 'fashion',
	'posts_per_page'  => 3,
	'paged'=>$paged
	);
$the_query = new WP_Query( $args );

$loop = 0;

if($the_query->post_count>0){
	
	$i	= 0;
	$col 	= 1;
	
	while ( $the_query->have_posts() ) : $the_query->the_post();
	$loop ++;

	if($i%$col===0){ echo ' <div class="row">';}
	
	?>
	<?php if($i != 0 ){?>
	<div class="space20"></div>
	<?php }?>
	<!--Pages Titles-->
	<div class="row">
		<div class="text-center">
			<p class="page-sub-titile"><?=the_title();?></p>
			<div class="col-md-1"></div>
			<div class="col-md-10"><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></div>
		</div>
		<div class="col-md-1"></div>	
	</div>
</div>

<div class="space20"></div>

<!--End Pages Titles-->



<div class="">
	<div id="carousel-example-generic<?=$loop?>" class="carousel slide" data-ride="carousel">
		<?php 
		$aserican_images = get_post_meta( $post->ID, 'aserica_image_slider', false);
                                        //print_r($aserican_images);
		?>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">

			<?php 
			$image_loop = 0;
			foreach ($aserican_images[0]  as $aserican_image_slider) :  
                                               //print_r($aserican_image_slider);
				?>
			<div class="item  <?php if($image_loop == 0){ echo "active"; } ?>">
				<a href="<?=the_permalink();?>">  <img class="img-responsive" src="<?=$aserican_image_slider?>"></a>
			</div>
			<?php 
			$image_loop++;
			endforeach;?>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic<?=$loop?>" data-slide="prev">
			<span class=""><<</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic<?=$loop?>" data-slide="next">
			<span class="">>></span>
		</a>
	</div>

	<div class="row">
		<div class="col-md-10">
			<div class="space20"></div>

			<div class="row">

				<div class="col-md-6">
					<span class="fashion-under-slide-title"><?=the_title()?></span> <br/>
					


				</div>
				<div class="col-md-5"></div>
			</div>

		</div>
		<div class="col-md-2"></div>

	</div>

	<div class="row">
		<div class="col-md-6">
			<span class=""><?=get_post_meta( $post->ID, 'aserica_photography_author',true);?></span>
			<br/>
			<?=get_post_meta( $post->ID, 'aserica_text_under_author',true);?>
		</div>
		<div class="col-md-6">
			<div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
		</div>
	</div>

	<div class="black-line"></div>
	<div class="line-normal-blod"></div>



	<?php

	if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
}

$i++;
$total_pages = $the_query->max_num_pages;
if ($total_pages != $paged) {
	$chang_page = $paged+1;
} else {
	$chang_page = $paged-1;
}
endwhile;
?>

<?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

<?php
}
	/* 
	 * Restore original Post Data
	 */
	wp_reset_postdata();
	?>
<!-- </div>

	<div class="container"> -->
		<!-- /.custom_post_types -->
		<div class="space20"></div>
		<!-- EXCLUSIVE -->
		<div class="row">
			<div class="col-md-2"><span class="big-bold"> <?=of_get_option('fashion_page_bold')?> </span>
				<br/>  
				<span class="content-front-page"> 
					<?=of_get_option('fashion_page_col')?>
				</span>
				<br/>
				<br/>
				<p>
					<span class="big-bold-yellow"> <a href="<?php echo get_site_url(); ?>/page-fashion/page/<?=$chang_page?>">MORE>> FASHION</a></span>
				</p>
			</div>

			<!-- Ramdom -->	
			<div class="col-md-10">

				<?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

				$args = array(
					'post_type'	    => 'fashion',
					'posts_per_page'  => 3,
					'orderby' => 'date',
					'order' => 'DESC',
					'paged'=>$paged+1
					);
				$the_query = new WP_Query( $args );

				if($the_query->post_count>0){

					$i	= 0;
					$col 	= 2;

					while ( $the_query->have_posts() ) : $the_query->the_post();
					if($i<2){
						if($i%$col===0){ echo ' <div class="row">';}

						?>

						<div class="col-md-<?php echo (12/$col); ?> <?php echo get_post_type(); ?>">

							<a href="<?=the_permalink();?>"> <?php  the_post_thumbnail('big-image', array('class' => 'img-responsive')); ?></a>
							<div class="space5"></div>
							<span class="news">FASHION</span><br/>
							<span class="page-the-title"><?php the_title();?> </span><br/>
							<span class="news"><?=get_post_meta( $post->ID, 'photography_author', true );?></span>
							<p><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></p> 

						</div>

						<?php

						if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
					}

					$i++;
				}
				endwhile;
				?>

				<?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

				<?php
			}
	/* 
	 * Restore original Post Data
	 */
	wp_reset_postdata();
	?>
</div>
</div>


<div class="row">
	<div class="col-md-12">
		<span class="front-page-under">FASHION</span>
		<br/>
		<?=of_get_option('fashion')?>
		<div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
	</div>

</div>
<div class="space10"></div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>

<!-- End EXCLUSIVE -->

<div class="space40"></div>
</div>
<?php get_footer(); ?>
