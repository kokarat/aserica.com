<?php
/**
 * @package aserica
 */
?>

<article id="post-<?php the_ID(); ?>" class="panel panel-default">
	<header class="panel-heading">
		<h1 class="panel-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="panel-body">
		<?php 
		if ( has_post_thumbnail() ) { 
			 the_post_thumbnail('thumbnail',array('class' => 'alignleft'));
		} 
		the_excerpt(); 
		?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="panel-body">
	<?php 
	            if ( is_singular() ) {
	                 the_content();
	            }else{
		// Doc http://codex.wordpress.org/Function_Reference/the_post_thumbnail
		if ( has_post_thumbnail() ) { 
			 the_post_thumbnail('thumbnail',array('class' => 'alignleft'));
		} 
	                the_excerpt();
	            }
	    ?>
	</div><!-- .panel-body -->
	<?php endif; ?>

	<footer class="panel-footer small">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'aserica' ) );
				if ( $categories_list && aserica_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( '<i class="fa fa-sitemap"></i> %1$s', 'aserica' ), $categories_list ); ?>
			</span>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'aserica' ) );
				if ( $tags_list ) :
			?>
			<span class="tags-links">
				<?php printf( __( '<i class="fa fa-tags"></i> %1$s', 'aserica' ), $tags_list ); ?>
			</span>
			<?php endif; // End if $tags_list ?>
		<?php endif; // End if 'post' == get_post_type() ?>

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'aserica' ), __( '1 Comment', 'aserica' ), __( '% Comments', 'aserica' ) ); ?></span>
		<?php endif; ?>

		<?php edit_post_link( __( 'Edit <i class="fa fa-pencil-square-o"></i>', 'aserica' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
