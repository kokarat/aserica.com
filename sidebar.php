<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package aserica
 */
?>
<div class="sidebar col-md-4">
	<aside id="archives" class="widget">
		<?php get_search_form();?>
	</aside>
	<?php if ( ! dynamic_sidebar( 'sidebar-primary-right' ) ) : ?>

		<aside id="archives" class="widget">
			<h3 class="widget-title"><?php _e( 'Archives', 'aserica' ); ?></h3>
			<ul>
				<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
			</ul>
		</aside>

		<aside id="meta" class="widget">
			<h3 class="widget-title"><?php _e( 'Meta', 'aserica' ); ?></h3>
			<ul>
				<?php wp_register(); ?>
				<li><?php wp_loginout(); ?></li>
				<?php wp_meta(); ?>
			</ul>
		<?php endif; // end sidebar widget area ?>
	</div><!--/sidebar-->
