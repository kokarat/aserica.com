<?php
/**
 * The template for displaying search forms in aserica
 *
 * @package aserica
 */
?>

<form role="form" method="get" class="form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="pull-right" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'aserica' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">	
</form>