<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package aserica
 */

get_header(); ?>


		<?php if ( have_posts() ) : ?>

			
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'aserica' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>

			<?php aserica_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>


<?php get_footer(); ?>
