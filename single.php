<?php
/**
 * The Template for displaying all single posts.
 *
 * @package aserica
 */

get_header(); ?>

<div class="row">
	<div class="col-md-8 panel-default">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php  aserica_post_nav(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() ) :
				comments_template();
			endif;
			?>

		<?php endwhile; // end of the loop. ?>

	</div><!-- col-md-8 -->
	<?php get_sidebar(); ?>
</div><!-- row -->
<?php get_footer(); ?>