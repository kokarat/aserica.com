<?php
/**
 * aserica functions and definitions
 *
 * @package aserica
 */

/**
 * Check PHP version
 */



/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 960; /* pixels */
}

if ( ! function_exists( 'aserica_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function aserica_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on aserica, use a find and replace
	 * to change 'aserica' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'aserica', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Doc http://codex.wordpress.org/Function_Reference/add_image_size
	 */
	 add_image_size( 'big-image', 850, 490, false ); 
	 add_image_size( 'medium-image', 420, 277, false ); 

	
	/**
	 * Custom functions that wp_bootstrap_navwalker independently of the theme templates
	 */
	require get_template_directory() . '/lib/wp_bootstrap_navwalker.php' ;

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'aserica_custom_background_args', array(
		'default-color' => 'E9EAED',
		'default-image' => '',
		) ) );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'aserica' ),
		'footer' => __( 'Footer Menu', 'aserica' ),
	) );
}
endif; // aserica_setup
add_action( 'after_setup_theme', 'aserica_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */

function aserica_widgets_init() {
	
	register_sidebar( array(
		'name' => __( 'Sidebar Primary Right', 'aserica' ),
		'id' => 'sidebar-primary-right',
		'before_widget' => '<aside id="%1$s" class="panel panel-default">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="panel-heading panel-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar Front Page', 'aserica' ),
		'id' => 'sidebar-front-page',
		'before_widget' => '<div id="%1$s" class="col-md-4 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );
	
	
	register_sidebar( array(
		'name' => __( 'Sidebar Footer', 'aserica' ),
		'id' => 'sidebar-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
}
add_action( 'widgets_init', 'aserica_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function aserica_scripts() {
	wp_enqueue_style( 'aserica-style', get_stylesheet_uri() );

	wp_enqueue_script( 'aserica-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'aserica-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'aserica_scripts' );


/*
 * Get Theme Option by aserica option id
 */
function aserica_get_option($aserica_option_id = ""){
	return do_shortcode('[aserica_option id="'.$aserica_option_id.'"]');
}

/**
 * Custom copyright text.
 */
function aserica_copyright(){
	echo "&copy; ".__( "Copyright", 'aserica' )." " . get_bloginfo( 'name' )." ". date("Y ") .". ".__( "All rights reserved.", 'aserica' );
}
 add_action('aserica_copyright','aserica_copyright');

/**
 * Control Excerpt Length using Filters
 * http://codex.wordpress.org/Function_Reference/the_excerpt
 */
function aserica_custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'aserica_custom_excerpt_length', 999 );

/**
 * Make the "read more" libk to the post
 * http://codex.wordpress.org/Function_Reference/the_excerpt
 */
function aserica_new_excerpt_more($more) {
    global $post;
	return ' <a class="readmore" href="'. get_permalink($post->ID) . '">'.__( ' <p class="pull-right"><button type="button" class="btn btn-default">Read More...</button></p>', 'aserica' ).'</a>';
}
add_filter('excerpt_more', 'aserica_new_excerpt_more');


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Post type.
 */
require get_template_directory() . '/lib/custom-post-types.php';







/**
* ==================
 * External Library Framwork  |
 *==================
 */

/**
 * Custom metaboxs
 * Resources : https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress
 */
require get_template_directory() . '/lib/custom-metaboxes.php';



/**
 * Theme Options
 * Resources : https://github.com/devinsays/options-framework-theme
 */

if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/lib/theme-options/inc/' );
	require get_template_directory() . '/lib/theme-options/inc/options-framework.php';
}

add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#example_showhidden').click(function() {
  		jQuery('#section-example_text_hidden').fadeToggle(400);
	});

	if (jQuery('#example_showhidden:checked').val() !== undefined) {
		jQuery('#section-example_text_hidden').show();
	}

});
</script>

<?php
}

// Custom Backend Footer
add_filter('admin_footer_text', 'bones_custom_admin_footer');
function bones_custom_admin_footer() {
	echo '<span>Engine by Wordpress <a href="http://www.wordpress.org" target="_blank">Wordpress</a></span> | <span id="footer-thankyou">Theme by <a href="http://www.kokarat.me" target="_blank">KOKARAT.ME</a></span>';
}

// adding it to the admin area
add_filter('admin_footer_text', 'bones_custom_admin_footer');