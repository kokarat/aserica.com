<?php
/**
 * The Template for displaying all single posts.
 *
 * @package aserica
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); 


$custom_fields = get_post_custom();

//print_r($custom_fields );
?>

	
	<div class="space20"></div>
	<!--Pages Titles-->
	<div class="row">
		<div class="text-center">
			<p class="page-title text-center">EXCLUSIVE</p>
			<p class="page-sub-titile"><?php the_title();?></p>
			<div class="col-md-1"></div>
			<div class="col-md-10"><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></div>
		</div>
		<div class="col-md-1"></div>	
	</div>

	<div class="space20"></div>

	<!--End Pages Titles-->


	<div class="row">
                        <div id="carousel-example-generic<?=$loop?>" class="carousel slide" data-ride="carousel">
                                <?php 
                                        $aserican_images = get_post_meta( $post->ID, 'aserica_image_slider', false);
                                        //print_r($aserican_images);
                                ?>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">

                                <?php 

                                $image_loop = 0;
                                        //foreach ($aserican_images[0]  as $aserican_image_slider) :  
                                        	foreach ($aserican_images[0] as $key => $value) :
                                               //print_r($aserican_image_slider);
                                                ?>
                                        <div class="item  <?php if($image_loop == 0){ echo "active"; } ?>">
				<?php
					$image_attributes = wp_get_attachment_image_src( $key ,'full');
				?>		

                                              <a href="<?=the_permalink();?>">  <img class="img-responsive" src="<?=$image_attributes[0]?>"></a>
                                        </div>
                                <?php 
                                $image_loop++;
                                endforeach;?>
                                </div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic<?=$loop?>" data-slide="prev">
		<span class=""><<</span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic<?=$loop?>" data-slide="next">
		<span class="">>></span>
	</a>
</div>

<div class="col-md-12">
	<div class="space20"></div>
	
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<span class="fashion-under-slide-title"><?=get_post_meta( $post->ID, 'aserica_photography_author', true );?></span>
			<p><?=wpautop(get_post_meta( $post->ID, 'aserica_content_text', true ));?></p>
			</div>
			<div class="col-md-1"></div>
		</div>

	</div>
</div>

<div class="space20"></div>
<div class="row">
	<div class="col-md-12"><p class="pull-right"><span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  <span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></p></div> 
</div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>

<?php endwhile; // end of the loop. ?>

<div class="space40"></div>
<?php get_footer(); ?>