<?php
add_filter( 'cmb_meta_boxes', 'aserica_configuration' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function aserica_configuration( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'aserica_';

	$meta_boxes['aserican_bold_text'] = array(
		'id'         => 'aserican_bold_text',
		'title'      => __( 'BOLD text Firstname and Lastname of Aserican', 'aserica'),
		'pages'      => array( 'aserica','introducing'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => __( 'Firstname', 'aserica'),
				'desc' => __( 'This text will show on BOLD on front page.', 'aserica'),
				'id'   => $prefix . 'bold_firstname',
				'type' => 'text_medium',
				// 'repeatable' => true,
				),

			array(
				'name' => __( 'Lastname', 'aserica'),
				'desc' => __( 'This text will show on BOLD on front page.', 'aserica'),
				'id'   => $prefix . 'bold_lastname',
				'type' => 'text_medium',
				// 'repeatable' => true,
				),			
			)
		);

	$meta_boxes['bold_text_front_page'] = array(
		'id'         => 'bold_text_front_page',
		'title'      => __( 'BOLD text for home page', 'aserica'),
		'pages'      => array('featuring','fashion','news','exclusive','day_and_places'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => __( 'BOLD text', 'aserica'),
				'desc' => __( 'This text will show on BOLD on home page.', 'aserica'),
				'id'   => $prefix . 'bold_text',
				'type' => 'text_medium',
				// 'repeatable' => true,
				),		
			)
		);


	$meta_boxes['text_custom'] = array(
		'id'         => 'text_custom',
		'title'      => __( 'Text Custom', 'aserica'),
		'pages'      => array( 'aserica','featuring','fashion','news','exclusive','introducing','day_and_places','page'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(

			array(
				'name' => __( 'Long text  ', 'aserica'),
				'desc' => __( 'This text will show beside BOLD on front page and Sub-title on content page.', 'aserica'),
				 'id'   => $prefix . 'long_text',
				'type' => 'textarea_code',
				// 
				// 'type'    => 'wysiwyg',
				// 'options' => array( 'textarea_rows' => 5, ),
				),

			array(
				'name' => __( 'Prefix Photography / Author', 'aserica'),
				'desc' => __( 'This text will show  prefix Photography / Author on content page.', 'aserica'),
				'id'   => $prefix . 'pre_photography_author',
				'type' => 'text_medium',
				// 'repeatable' => true,
				),

			array(
				'name' => __( 'Photography / Author', 'aserica'),
				'desc' => __( 'This text will show on content page', 'aserica'),
				'id'   => $prefix . 'photography_author',
				'type' => 'text_medium',
				// 'repeatable' => true,
				),
			array(
				'name'    => __( 'Text under Photography / Author', 'aserica' ),
				'desc'    => __( 'This text will show on content page', 'aserica' ),
				'id'      => $prefix . 'text_under_author',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 5, ),
				),			
			)
);

/*
$meta_boxes['image_and_text'] = array(
	'id'         => 'image_and_text',
	'title'      => __( 'Image With text for front page', 'aserica'),
		'pages'      => array( 'aserica','featuring','fashion','news','exclusive','introducing','day_and_places'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => __( 'Image with text', 'aserica'),
				'desc' => __( 'Upload an image or enter a URL.', 'aserica'),
				'id'   => $prefix . 'image_and_text',
				'type' => 'file',
				),
			)
		);
*/

// Cover page
// 
$meta_boxes['cover_image'] = array(
	'id'         => 'cover_image',
	'title'      => __( 'Image With text for Cover page', 'aserica'),
		'pages'      => array( 'coverpage',), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => __( 'Image with text', 'aserica'),
				'desc' => __( 'Upload an image or enter a URL (size recommend width min 2000px high 800px)', 'aserica'),
				'id'   => $prefix . 'cover_image',
				'type' => 'file',
				),
			)
		);

//Aserica Slide
//
//
$meta_boxes['aserica_slide'] = array(
	'id'         => 'cover_image',
	'title'      => __( 'Image With text for Aserica slide', 'aserica'),
		'pages'      => array( 'aserica_slide','day_and_places_slide'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => __( 'Image with text', 'aserica'),
				'desc' => __( 'Upload an image or enter a URL (size recommend width min 2000px high 800px)', 'aserica'),
				'id'   => $prefix . 'aserica_slide',
				'type' => 'file',
				),
			array(
				'name' => __( 'Link to page', 'aserica'),
				'desc' => __( 'Put link to Aserica content page', 'aserica'),
				'id'   => $prefix . 'aserica_slide_link',
				'type' => 'text',
				// 'repeatable' => true,
				),			
			)
		);


//Images for  content page (slide)
//
$meta_boxes['image_slider'] = array(
	'id'         => 'image_slider',
	'title'      => __( 'Images slider for content page', 'aserica'),
		'pages'      => array( 'featuring','aserica','fashion','news','exclusive','introducing','day_and_places'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(

			array(
				'name' => __( 'Images (multiple)', 'aserica'),
				'desc' => __( 'Upload or add multiple images/attachments.', 'aserica'),
				'id'   => $prefix . 'image_slider',
				'type' => 'file_list',
				),
			)
		);


//Content text
//

$meta_boxes['content_text'] = array(
	'id'         => 'content_text',
	'title'      => __( 'Content text', 'aserica'),
		'pages'      => array( 'featuring','aserica','fashion','news','exclusive','introducing','day_and_places'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(

			array(
				'name'    => __( 'Content', 'aserica' ),
				'desc'    => __( 'This text will show on content page', 'aserica' ),
				'id'      => $prefix . 'content_text',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 5, ),
				),	
			)
		);



$meta_boxes['fashion_text'] = array(
	'id'         => 'fashion_text',
	'title'      => __( 'Text under bold', 'aserica'),
		'pages'      => array( 'fashion'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(

			array(
				'name'    => __( 'Text under bold', 'aserica' ),
				'desc'    => __( 'This text will show under bold on fashion page', 'aserica' ),
				'id'      => $prefix . 'fashion_text',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 5, ),
				),	
			)
		);


// aserica slide
// 
// $meta_boxes['aserica_slide_image'] = array(
// 	'id'         => 'aserica_slide_image',
// 	'title'      => __( 'Image for aserica slide', 'aserica'),
// 		'pages'      => array( 'aserica'), // Post type
// 		'context'    => 'normal',
// 		'priority'   => 'high',
// 		'show_names' => true, // Show field names on the left
// 		'fields' => array(
// 			array(
// 				'name' => __( 'Image  for aserica slide', 'aserica'),
// 				'desc' => __( 'Upload an image or enter a URL (size recommend width min 2000px high 800px)', 'aserica'),
// 				'id'   => $prefix . 'aserica_slide_image',
// 				'type' => 'file',
// 				),
// 			)
// 		);

	// Add other metaboxes as needed

return $meta_boxes;
}


add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once get_template_directory() .'/lib/custom-metaboxes/init.php';

}