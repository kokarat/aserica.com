<?php
// Register Custom Post Type
function custom_post_types() {
	
	// COVER PAGE
	$labels = array(
		'name'                => _x( 'Cover page', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'Cover page', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'Cover page', 'aserica' ),
		'parent_item_colon'   => __( 'Parent Cover page:', 'aserica' ),
		'all_items'           => __( 'All Cover pages', 'aserica' ),
		'view_item'           => __( 'View Cover page', 'aserica' ),
		'add_new_item'        => __( 'Add New Cover page', 'aserica' ),
		'add_new'             => __( 'New Cover page', 'aserica' ),
		'edit_item'           => __( 'Edit Cover page', 'aserica' ),
		'update_item'         => __( 'Update Cover page', 'aserica' ),
		'search_items'        => __( 'Search Cover pages', 'aserica' ),
		'not_found'           => __( 'No Cover pages found', 'aserica' ),
		'not_found_in_trash'  => __( 'No Cover pages found in Trash', 'aserica' ),
		);
	$args = array(
		'label'               => __( 'Cover page', 'aserica' ),
		'description'         => __( 'Cover page information pages', 'aserica' ),
		'labels'              => $labels,
		'supports'            => array( 'title'),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 101,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/Cover pages.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'coverpage', $args );

	// FEATRING
	$labels = array(
		'name'                => _x( 'FEATRING', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'FEATRING', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'FEATRING', 'aserica' ),
		'parent_item_colon'   => __( 'Parent FEATRING:', 'aserica' ),
		'all_items'           => __( 'All FEATRING', 'aserica' ),
		'view_item'           => __( 'View FEATRING', 'aserica' ),
		'add_new_item'        => __( 'Add New FEATRING', 'aserica' ),
		'add_new'             => __( 'New FEATRING', 'aserica' ),
		'edit_item'           => __( 'Edit FEATRING', 'aserica' ),
		'update_item'         => __( 'Update FEATRING', 'aserica' ),
		'search_items'        => __( 'Search FEATRING', 'aserica' ),
		'not_found'           => __( 'No FEATRING found', 'aserica' ),
		'not_found_in_trash'  => __( 'No FEATRING found in Trash', 'aserica' ),
		);
	$args = array(
		'label'               => __( 'FEATRING', 'aserica' ),
		'description'         => __( 'FEATRING information pages', 'aserica' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 101,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/Cover pages.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'featuring', $args );

	// ASERICAN
	$labels_aserica = array(
		'name'                => _x( 'ASERICAN', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'ASERICAN', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'ASERICAN', 'aserica' ),
		'parent_item_colon'   => __( 'Parent ASERICAN:', 'aserica' ),
		'all_items'           => __( 'All ASERICAN', 'aserica' ),
		'view_item'           => __( 'View ASERICAN', 'aserica' ),
		'add_new_item'        => __( 'Add New ASERICAN', 'aserica' ),
		'add_new'             => __( 'New ASERICAN', 'aserica' ),
		'edit_item'           => __( 'Edit ASERICAN', 'aserica' ),
		'update_item'         => __( 'Update ASERICAN', 'aserica' ),
		'search_items'        => __( 'Search ASERICANs', 'aserica' ),
		'not_found'           => __( 'No ASERICANs found', 'aserica' ),
		'not_found_in_trash'  => __( 'No ASERICANs found in Trash', 'aserica' ),
		);
	$args_aserica = array(
		'label'               => __( 'ASERICAN', 'aserica' ),
		'description'         => __( 'ASERICAN information pages', 'aserica' ),
		'labels'              => $labels_aserica,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 102,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'aserica', $args_aserica );

	// ASERICAN SLIDE
	$labels_aserica = array(
		'name'                => _x( 'ASERICAN SLIDE', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'ASERICAN SLIDE', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'ASERICAN SLIDE', 'aserica' ),
		'parent_item_colon'   => __( 'Parent ASERICAN SLIDE:', 'aserica' ),
		'all_items'           => __( 'All ASERICAN SLIDE', 'aserica' ),
		'view_item'           => __( 'View ASERICAN SLIDE', 'aserica' ),
		'add_new_item'        => __( 'Add New ASERICAN SLIDE', 'aserica' ),
		'add_new'             => __( 'New ASERICAN SLIDE', 'aserica' ),
		'edit_item'           => __( 'Edit ASERICAN SLIDE', 'aserica' ),
		'update_item'         => __( 'Update ASERICAN SLIDE', 'aserica' ),
		'search_items'        => __( 'Search ASERICAN SLIDE', 'aserica' ),
		'not_found'           => __( 'No ASERICAN SLIDE found', 'aserica' ),
		'not_found_in_trash'  => __( 'No ASERICAN SLIDE found in Trash', 'aserica' ),
		);
	$args_aserica = array(
		'label'               => __( 'ASERICAN SLIDE', 'aserica' ),
		'description'         => __( 'ASERICAN SLIDE information pages', 'aserica' ),
		'labels'              => $labels_aserica,
		'supports'            => array( 'title', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 102,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	//register_post_type( 'aserica_slide', $args_aserica );	

	// FASHION
	$labels_fashion = array(
		'name'                => _x( 'FASHION', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'FASHION', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'FASHION', 'aserica' ),
		'parent_item_colon'   => __( 'Parent FASHION:', 'aserica' ),
		'all_items'           => __( 'All FASHIONs', 'aserica' ),
		'view_item'           => __( 'View FASHION', 'aserica' ),
		'add_new_item'        => __( 'Add New FASHION', 'aserica' ),
		'add_new'             => __( 'New FASHION', 'aserica' ),
		'edit_item'           => __( 'Edit FASHION', 'aserica' ),
		'update_item'         => __( 'Update FASHION', 'aserica' ),
		'search_items'        => __( 'Search FASHIONs', 'aserica' ),
		'not_found'           => __( 'No FASHIONs found', 'aserica' ),
		'not_found_in_trash'  => __( 'No FASHIONs found in Trash', 'aserica' ),
		);
	$args_fashion = array(
		'label'               => __( 'FASHION', 'aserica' ),
		'description'         => __( 'FASHION information pages', 'aserica' ),
		'labels'              => $labels_fashion,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 103,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'fashion', $args_fashion);


	// NEWS
	$labels_news = array(
		'name'                => _x( 'NEWS', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'NEWS', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'NEWS', 'aserica' ),
		'parent_item_colon'   => __( 'Parent NEWS:', 'aserica' ),
		'all_items'           => __( 'All NEWS', 'aserica' ),
		'view_item'           => __( 'View NEWS', 'aserica' ),
		'add_new_item'        => __( 'Add New NEWS', 'aserica' ),
		'add_new'             => __( 'New NEWS', 'aserica' ),
		'edit_item'           => __( 'Edit NEWS', 'aserica' ),
		'update_item'         => __( 'Update NEWS', 'aserica' ),
		'search_items'        => __( 'Search NEWS', 'aserica' ),
		'not_found'           => __( 'No NEWS found', 'aserica' ),
		'not_found_in_trash'  => __( 'No NEWS found in Trash', 'aserica' ),
		);
	$args_news = array(
		'label'               => __( 'NEWS', 'aserica' ),
		'description'         => __( 'NEWS information pages', 'aserica' ),
		'labels'              => $labels_news,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 104,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'news', $args_news);


	// EXCLUSIVE
	$labels_exclusive = array(
		'name'                => _x( 'EXCLUSIVE', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'EXCLUSIVE', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'EXCLUSIVE', 'aserica' ),
		'parent_item_colon'   => __( 'Parent EXCLUSIVE:', 'aserica' ),
		'all_items'           => __( 'All EXCLUSIVE', 'aserica' ),
		'view_item'           => __( 'View EXCLUSIVE', 'aserica' ),
		'add_new_item'        => __( 'Add New EXCLUSIVE', 'aserica' ),
		'add_new'             => __( 'New EXCLUSIVE', 'aserica' ),
		'edit_item'           => __( 'Edit EXCLUSIVE', 'aserica' ),
		'update_item'         => __( 'Update EXCLUSIVE', 'aserica' ),
		'search_items'        => __( 'Search EXCLUSIVE', 'aserica' ),
		'not_found'           => __( 'No EXCLUSIVE found', 'aserica' ),
		'not_found_in_trash'  => __( 'No EXCLUSIVE found in Trash', 'aserica' ),
		);
	$args_exclusive = array(
		'label'               => __( 'EXCLUSIVE', 'aserica' ),
		'description'         => __( 'EXCLUSIVE information pages', 'aserica' ),
		'labels'              => $labels_exclusive,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 105,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'exclusive', $args_exclusive);



	// INTRODUCING
	$labels_introducing = array(
		'name'                => _x( 'INTRODUCING', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'INTRODUCING', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'INTRODUCING', 'aserica' ),
		'parent_item_colon'   => __( 'Parent INTRODUCING:', 'aserica' ),
		'all_items'           => __( 'All INTRODUCING', 'aserica' ),
		'view_item'           => __( 'View INTRODUCING', 'aserica' ),
		'add_new_item'        => __( 'Add New INTRODUCING', 'aserica' ),
		'add_new'             => __( 'New INTRODUCING', 'aserica' ),
		'edit_item'           => __( 'Edit INTRODUCING', 'aserica' ),
		'update_item'         => __( 'Update INTRODUCING', 'aserica' ),
		'search_items'        => __( 'Search INTRODUCING', 'aserica' ),
		'not_found'           => __( 'No INTRODUCING found', 'aserica' ),
		'not_found_in_trash'  => __( 'No INTRODUCING found in Trash', 'aserica' ),
		);
	$args_introducing = array(
		'label'               => __( 'INTRODUCING', 'aserica' ),
		'description'         => __( 'INTRODUCING information pages', 'aserica' ),
		'labels'              => $labels_introducing,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 106,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'introducing', $args_introducing);


	// DAYS AND PLACES
	$labels_day_and_places = array(
		'name'                => _x( 'DAYS & PLACES', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'DAYS & PLACES', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'DAYS & PLACES', 'aserica' ),
		'parent_item_colon'   => __( 'Parent DAYS & PLACES:', 'aserica' ),
		'all_items'           => __( 'All DAYS & PLACES', 'aserica' ),
		'view_item'           => __( 'View DAYS & PLACES', 'aserica' ),
		'add_new_item'        => __( 'Add New DAYS & PLACES', 'aserica' ),
		'add_new'             => __( 'New DAYS & PLACES', 'aserica' ),
		'edit_item'           => __( 'Edit DAYS & PLACES', 'aserica' ),
		'update_item'         => __( 'Update DAYS & PLACES', 'aserica' ),
		'search_items'        => __( 'Search DAYS & PLACES', 'aserica' ),
		'not_found'           => __( 'No DAYS & PLACES found', 'aserica' ),
		'not_found_in_trash'  => __( 'No DAYS & PLACES found in Trash', 'aserica' ),
		);
	$args_day_and_places = array(
		'label'               => __( 'DAYS & PLACES', 'aserica' ),
		'description'         => __( 'DAYS & PLACES information pages', 'aserica' ),
		'labels'              => $labels_day_and_places,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 107,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	register_post_type( 'day_and_places', $args_day_and_places);


	// DAYS AND PLACES SLIDE
	$labels_day_and_places_slide = array(
		'name'                => _x( 'DAYS&PLACES SLIDE', 'Post Type General Name', 'aserica' ),
		'singular_name'       => _x( 'DAYS & PLACES', 'Post Type Singular Name', 'aserica' ),
		'menu_name'           => __( 'DAYS&PLACES SLIDE', 'aserica' ),
		'parent_item_colon'   => __( 'Parent DAYS & PLACES:', 'aserica' ),
		'all_items'           => __( 'All DAYS & PLACES', 'aserica' ),
		'view_item'           => __( 'View DAYS & PLACES', 'aserica' ),
		'add_new_item'        => __( 'Add New DAYS & PLACES', 'aserica' ),
		'add_new'             => __( 'New DAYS & PLACES', 'aserica' ),
		'edit_item'           => __( 'Edit DAYS & PLACES', 'aserica' ),
		'update_item'         => __( 'Update DAYS & PLACES', 'aserica' ),
		'search_items'        => __( 'Search DAYS & PLACES', 'aserica' ),
		'not_found'           => __( 'No DAYS & PLACES found', 'aserica' ),
		'not_found_in_trash'  => __( 'No DAYS & PLACES found in Trash', 'aserica' ),
		);
	$args_day_and_places_slide = array(
		'label'               => __( 'DAYS&PLACES SLIDE', 'aserica' ),
		'description'         => __( 'DAYS & PLACES information pages', 'aserica' ),
		'labels'              => $labels_day_and_places_slide,
		'supports'            => array( 'title', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 107,
		'menu_icon'           => 'https://lh4.googleusercontent.com/-i0Cb2MWXqzY/Usg8WVZ7NJI/AAAAAAAABCs/nH3J1cNEQG4/s16-no/ASERICANs.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
	//register_post_type( 'day_and_places_slide', $args_day_and_places_slide);



}

// Hook into the 'init' action
add_action( 'init', 'custom_post_types', 0 );
?>