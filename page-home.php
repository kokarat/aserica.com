<?php
/**
 * Template Name: HOME
 *
 * @package aserica
 */

get_header(); ?>

<?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query
$args = array(
    'post_type'         => 'featuring',
    'posts_per_page'  => 1
);
$the_query = new WP_Query( $args );

if($the_query->post_count>0){

    while ( $the_query->have_posts() ) : $the_query->the_post();
        ?>

        <?php //$getPostCustom=get_post_custom(); // Get all the data ?>


        <div class="row">
            <div class="col-md-9"> <?php  the_post_thumbnail('full', array('class' => 'img-responsive')); ?></div>
            <div class="col-md-3"><span class="big-bold"> <?=get_post_meta( $post->ID, 'aserica_bold_text', true );?> </span> <br/><span class="content-front-page"> <?=iconv_substr(wpautop(get_post_meta( $post->ID, 'aserica_content_text', true )),0,950, "UTF-8")."...";?>
    </span>
                <br/>
                <br/>
                <p>
                    <span class="big-bold-yellow"> <a href="<?=the_permalink();?>">SEE>> </a></span>
                </p>
            </div>
        </div>
        <div class="space20"></div>
        <div class="row">
            <div class="col-md-12">
                <span class=""> FEATURING  </span>
                <br/>
                <?php echo of_get_option('featuring')?>
                <div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
            </div>
        </div>

    <?php
    endwhile;
    ?>

<?php
}
/*
 * Restore original Post Data
 */
wp_reset_postdata();
?>

        <div class="space5"></div>
        <div class="black-line"></div>
        <div class="line-normal-blod"></div>

        <!-- End Featuring -->

        <!-- ASERICAN-->
        <div class="space20"></div>
        <div class="row">
          <a href="<?php echo get_site_url(); ?>/page-aserican">
          <?php echo do_shortcode("[metaslider id=900]");?></a>
          </div>

          <div class="space20"></div>
<!-- Acerican -->
  <div class="row">
    <div class="col-md-3"><span class="big-bold"> ASERICAN<?//=of_get_option('bold_col_exclusive')?>  </span> <br/><span class="content-front-page">From The East and The West - Aserican is a New Citizenship. L.A Tokyo Moscow London Shanghai Bucharest Bangkok Milan NewYorkCity Seoul Paris Zurich Madrid Berlin Beijing. Aserican are from Everywhere. Regular Contributors or One Night Stands /Summer Romances/Muses/Fashionistas /Film Makers /Watch /Discover & Follow.<?//=wpautop(of_get_option('exclusive_col'))?></span>
      <br/>
      <br/>
      <span class="big-bold-yellow"> <a href="<?php echo get_site_url(); ?>/page-aserican/">SEE>></a></span>
    </div>
    <div class="col-md-9">


      <?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

$args = array(
    'post_type'         => 'aserica',
    'order'             => 'DESC',
    'posts_per_page'  => 2
);
$the_query = new WP_Query( $args );

if($the_query->post_count>0){

    $i      = 0;
    $col    = 2;

    while ( $the_query->have_posts() ) : $the_query->the_post();

        if($i%$col===0){ echo ' <div class="row">';}

        ?>

        <div class="col-md-<?php echo (12/$col); ?>">

            <a href="<?=the_permalink();?>"> <?php  the_post_thumbnail('big-image', array('class' => 'img-responsive')); ?></a>
            <div class="space5"></div>

            <span class="news">ASERICAN:</span><br/>
            <span class="news"><?/*=get_post_meta( $post->ID, 'aserica_photography_author',true );*/  the_title();?></span>
            <p><span class="content-front-page"><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></span></p>

        </div>

        <?php

        if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
        }

        $i++;

    endwhile;
    ?>

    <?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

<?php
}
/*
 * Restore original Post Data
 */
wp_reset_postdata();
?>

      </div>
    </div>
          <div class="space20"></div>
          <div class="row">
            <div class="col-md-12">
              <span class="">ASERICAN</span>
              <br/>
              <?php echo of_get_option('aserican')?>
              <div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode('http://aserica.com/page-exclusive')?>">SHARE</a></span></div>
            </div>     
          </div>

          <div class="space5"></div>
          <div class="black-line"></div>
          <div class="line-normal-blod"></div>

          <!-- End ASERICAN -->


          <!-- Fashion -->
          <div class="space20"></div>

          <?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

$args = array(
    'post_type'         => 'fashion',
    'order'             => 'DESC',
    'posts_per_page'  => 1
);
$the_query = new WP_Query( $args );

if($the_query->post_count>0){

    while ( $the_query->have_posts() ) : $the_query->the_post();
        ?>

        <div class="row">
            <div class="col-md-3"><span class="big-bold"> <?=of_get_option('bold_col_fashion')?></span> <br/><span class="content-front-page"> <?=wpautop(of_get_option('col_fashion'))?>
              </span>
                <br/>
                <br/>
                <span class="big-bold-yellow"><a href="<?php echo get_site_url(); ?>/page-fashion"> MORE>> FASHION</a></span>
            </div>
            <div class="col-md-9"><a href="<?php echo get_site_url(); ?>/page-fashion"> <?php  the_post_thumbnail('full', array('class' => 'img-responsive')); ?></a></div>
        </div>

        <div class="space20"></div>

        <div class="row">
            <div class="col-md-12">
                <span class="">FASHION</span>
                <br/>
                <?=of_get_option('fashion')?>
                <div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
            </div>
        </div>
    <?php
    endwhile;
    ?>
<?php
}
/*
 * Restore original Post Data
 */
wp_reset_postdata();
?>




        <div class="space5"></div>
        <div class="black-line"></div>
        <div class="line-normal-blod"></div>

        <!-- End Fashion -->


        <!-- News -->
        <div class="space20"></div>

        <div class="row">
          <div class="col-md-9">

            <?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

$args = array(
    'post_type'         => 'news',
    'order'             => 'DESC',
    'posts_per_page'  => 6
);
$the_query = new WP_Query( $args );

if($the_query->post_count>0){

    $i      = 0;
    $col    = 3;

    while ( $the_query->have_posts() ) : $the_query->the_post();

        if($i%$col===0){ echo ' <div class="row">';}

        ?>

        <div class="col-md-<?php echo (12/$col); ?> <?php echo get_post_type(); ?>">

            <a href="<?=the_permalink();?>"> <?php  the_post_thumbnail('big-image', array('class' => 'img-responsive')); ?></a>
            <div class="space5"></div>

            <span class="news">NEWS</span><br/>
            <span class="news"><?/*=get_post_meta( $post->ID, 'aserica_photography_author',true ) ;*/ the_title();?></span>
            <p><span class="content-front-page"><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></span></p>

        </div>

        <?php

        if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
        }

        $i++;

    endwhile;
    ?>

    <?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

<?php
}
/*
 * Restore original Post Data
 */
wp_reset_postdata();
?>
        
      </div>



      <div class="col-md-3"><span class="big-bold"> <?=of_get_option('bold_col_news')?> </span> <br/><span class="content-front-page"> <?=wpautop(of_get_option('news_col'))?>
      </span>
      <br/>
      <br/>
      <span class="big-bold-yellow"><a href="<?php echo get_site_url(); ?>/page-news"> MORE>> <br/> NEWS</a></span>
    </div>
  </div>

  <div class="space20"></div>

  <div class="row">
    <div class="col-md-12">
      <span class="">NEWS</span>
      <br/>
      <?php echo of_get_option('news')?>
      <div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode('http://aserica.com/page-news')?>">SHARE</a></span></div>
    </div>
  </div>
  <div class="space5"></div>
  <div class="black-line"></div>
  <div class="line-normal-blod"></div>

  <!-- End News -->

  <div class="space20"></div>

  <!-- EXCLUSIVE -->
  <div class="row">
    <div class="col-md-3"><span class="big-bold"> <?=of_get_option('bold_col_exclusive')?>  </span> <br/><span class="content-front-page"><?=wpautop(of_get_option('exclusive_col'))?></span>
      <br/>
      <br/>
      <span class="big-bold-yellow"> <a href="<?php echo get_site_url(); ?>/page-exclusive">SEE>></a></span>
    </div>
    <div class="col-md-9">


      <?php

// Doc :  http://codex.wordpress.org/Class_Reference/WP_Query

$args = array(
    'post_type'         => 'exclusive',
    'order'             => 'DESC',
    'posts_per_page'  => 2
);
$the_query = new WP_Query( $args );

if($the_query->post_count>0){

    $i      = 0;
    $col    = 2;

    while ( $the_query->have_posts() ) : $the_query->the_post();

        if($i%$col===0){ echo ' <div class="row">';}

        ?>

        <div class="col-md-<?php echo (12/$col); ?>">

            <a href="<?=the_permalink();?>"> <?php  the_post_thumbnail('big-image', array('class' => 'img-responsive')); ?></a>
            <div class="space5"></div>

            <span class="news">EXCLUSIVE:</span><br/>
            <span class="news"><?/*=get_post_meta( $post->ID, 'aserica_photography_author',true );*/  the_title();?></span>
            <p><span class="content-front-page"><?=get_post_meta( $post->ID, 'aserica_long_text', true );?></span></p>

        </div>

        <?php

        if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
        }

        $i++;

    endwhile;
    ?>

    <?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>

<?php
}
/*
 * Restore original Post Data
 */
wp_reset_postdata();
?>

      </div>
    </div>
    <div class="space20"></div>

    <div class="row">
      <div class="col-md-12">
        <span class="">EXCLUSIVE </span>
        <br/>
        <?php echo of_get_option('exclusive')?>
        <div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode('http://aserica.com/page-exclusive')?>">SHARE</a></span></div>
      </div>
    </div>
    <div class="space5"></div>
    <div class="black-line"></div>
    <div class="line-normal-blod"></div>

    <!-- End EXCLUSIVE -->
    <div class="space20"></div>

    <!-- DAYS & PLACES -->



    <div class="row">
      <div class="col-md-9"><a href="<?php echo get_site_url(); ?>/page-days-places/"><?php
echo do_shortcode("[metaslider id=903]");
?></a> </div>
        <div class="col-md-3"><span class="big-bold"> <?=of_get_option('bold_col_days_and_places')?></span><br/> <span class="content-front-page"> <?=wpautop(of_get_option('col_days_and_places'))?></span>
        <br/>
        <br/>
        <span class="big-bold-yellow"> <a href="<?php echo get_site_url(); ?>/page-days-places">SEE>> </a></span>
      </div>
    </div>
    <div class="space20"></div>
    <div class="row">
      <div class="col-md-12">
        <span class="">DAYS & PLACES</span>
        <br/>
        <?php echo of_get_option('days_and_places')?>
        <div class="pull-right botton-up"> <span class="about-share"><a href="/page-about-us/">ABOUT US</a></span>  &nbsp;<span class="about-share"><a href="http://www.facebook.com/sharer/sharer.php?u=<?=urlencode(get_permalink($post->ID ))?>" target="_blank">SHARE</a></span></div>
      </div>
    </div>


    <div class="space5"></div>
    <div class="black-line"></div>
    <div class="line-normal-blod"></div>

    <!-- End DAYS & PLACES -->

    <div class="space40"></div>
  </div>

  <?php get_footer(); ?>