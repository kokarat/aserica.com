<?php
/**
 * The Template for displaying all single posts.
 *
 * @package aserica
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	
	<div class="space20"></div>
	<!--Pages Titles-->
	<div class="row">
		<div class="text-center">
			<p class="page-title text-center">FASHION</p>
			<p class="page-sub-titile">BAD GIRL BKK 1962 By sinn ward</p>
			<div class="col-md-1"></div>
			<div class="col-md-10">Tellus ut adipiscing imperdiet, ante odio pulvinar diam, in dignissim tellus nisl sed leo. Vivamus sagittis vestibulum mi, sit amet varius lectus ultrices at. hendrerit turpis pulvinar. Photographs by Sinn Ward, Fashion Editor Lu Jiaying.</div>
		</div>
		<div class="col-md-1"></div>	
	</div>

	<div class="space20"></div>

	<!--End Pages Titles-->


	<div class="row">
		<div id="carousel-example-generic<?=$loop?>" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
<!-- 		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		</ol>
	-->
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item  active">
			<img class="text-center" src="http://fakeimg.pl/1200x400/" alt="...">
		</div>
		<div class="item">
			<img src="http://fakeimg.pl/1200x400/" alt="...">
		</div>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic<?=$loop?>" data-slide="prev">
		<span class=""><<</span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic<?=$loop?>" data-slide="next">
		<span class="">>></span>
	</a>
</div>

<div class="col-md-12">
	<div class="space20"></div>
	
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<span class="fashion-under-slide-title">BAD GIRL BKK 1962</span> <br/>
			<span class="fashion-under-slide-title">Photography Sinn Ward, Fashion Editor Lu Jiaying</span>
			<p>Enas alesuada eget ullamcorper ante egestas. tellus ut adipiscing imperdiet, ante odio pulvinar diam, in dignissim tellus nisl sed leo. Ethnic Raw Silk Jacket by Comme Des Garçons.</p>
			<br/>

			<p>Make Up Charlie Grenn For Shiseido
				Hair David Mallett At Airport
				Models Anastasia Khodoneva at Streeters, Alexandra Strosi at Viva Paris. Assistants Duke & Liisa at Premier Management
				Studio Daylight Paris.</p>

			</div>
			<div class="col-md-1"></div>
		</div>

	</div>
</div>

<div class="space20"></div>
<div class="row">
	<div class="col-md-12"><p class="pull-right"><span class="about-share">ABOUT US</span>  <span class="about-share">SHARE</span></p></div> 
</div>
<div class="black-line"></div>
<div class="line-normal-blod"></div>

<?php endwhile; // end of the loop. ?>

<div class="space40"></div>
<?php get_footer(); ?>