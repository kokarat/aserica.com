<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package aserica
 */
?>

</div><!-- #content container -->

<?php wp_nav_menu( array( 'theme_location' => 'footer','menu_id' => 'footer-menu','fallback_cb' => '','container' => 'ul', ) ); ?>

<footer id="colophon" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="text-center">

					<?php if(of_get_option('footer_image_logo')) { ?>
					<img class="i" src="<?php echo of_get_option('footer_image_logo'); ?>" alt="">
					<?php } else{?>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-gold.png" alt="">
					<?php }?>
				</p>
			</div>
<!-- 				<p class="text-center footer-menu">
					HOME /// ASERICAN /// FASHION /// NEWS /// INTRODUCING /// DAYS AND PLACES /// MORE /// ABOUT US
				</p> -->
				<div class="text-center footer-menu">
					<li><a href="<?php echo get_site_url(); ?>/page-home">HOME</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-aserican">ASERICAN</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-fashion">FASHION</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-news">NEWS</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-exclusive">EXCLUSIVE</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-introducing">INTRODUCING</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-days-places">DAYS AND PLACES</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-more">MORE</a></li>
					<li><a href="<?php echo get_site_url(); ?>/page-about-us">ABOUT US</a></li>
				</div>
				<div class="space5"></div>
				<div class="gold-line"></div>

				<div class="row">
					<div class="col-md-3">	
						<span class="subscribe-footer"><a href="<?php echo get_site_url(); ?>/page-more">SUBSCRIBE TO ASERICA</a></span>
					</div>
					<div class="col-md-3"></div>
					<div class="col-md-4">
						<div class="footer-social">
							<span class="pull-right"><a href="<?php echo of_get_option('facebook'); ?>" target="_blank">FACEBOOK </a>/ <a href="<?php echo of_get_option('twitter'); ?>" target="_blank">TWITTER </a>/  <a href="<?php echo of_get_option('instagram'); ?>" target="_blank">INSTAGRAM </a> /<a href="mailto:info@aserica.com" target="_blank">CONTACT </a></span>
						</div>
					</div>
					<div class="col-md-2 footer-social">
						<form role="form" method="get" class="pull-right" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="text" class="pull-right input-black" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'aserica' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">	
						</form>
					</div>

				</div>


				<!-- <p class="pull-left subscribe-footer">SUBSCRIBE TO ASERICA</p>
				<p class="pull-right footer-social"><a href="<?php echo of_get_option('facebook'); ?>">FACEBOOK </a>/ <a href="<?php echo of_get_option('twitter'); ?>">TWITTER </a>/ <a href="/page-contact">CONTACT </a></p> -->
			</div><!-- .col-md-12 -->
			<div class="space20"></div>
			<div class="row">
				<div class="text-long-footer">
					<?php echo of_get_option('footer_text')?>
				</div>
			</div>
		</div><!-- #footer container -->
	</footer><!-- #colophon -->


	<!-- jQuery -->
	<script src="http://code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

	<script type="text/javascript">

		$('#spnbottom').on("click", function () {
			var percentageToScroll = 20;
			var percentage = percentageToScroll / 100;
			var height = $(document).height() - $(window).height();
			var scrollAmount = height * percentage;
			console.log('scrollAmount: ' + scrollAmount);
			jQuery("html, body").animate({
				scrollTop: scrollAmount
			}, 900);
		});

	</script>
	

	<script type="text/javascript">
		$('.carousel').carousel({
			interval: false
		})
	</script>


	<?php wp_footer(); ?>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-52170487-1', 'aserica.com');
		ga('send', 'pageview');

	</script>
</body>
</html>